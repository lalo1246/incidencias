
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="<?=BASE_URL?>js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?=BASE_URL?>js/bootstrap.min.js"></script>
  <!-- DataTables -->
  <script src="<?=BASE_URL?>datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?=BASE_URL?>datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=BASE_URL?>js/app.js"></script>
  </body>
</html>
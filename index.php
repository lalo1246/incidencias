
<?php
session_start();
include 'conexion.php';

  if(isset($_SESSION['id_usuario']))
    header('location:inicio.php');
    
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?=BASE_URL?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URL?>css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=BASE_URL?>datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <base href="<?=BASE_URL?>">
  </head>
  <body>
    <br><br><br><br><br><br><br><br>
    <form class="well col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 " method="POST" action="login.php">
      <div class="form-group">
        <label>Usuario</label>
        <input type="text" class="form-control" name="Usuario" >
      </div>
      <div class="form-group">
        <label>Contraseña</label>
        <input type="password" class="form-control" name="Password" >
      </div>
      <input type="submit" name="login" class="btn btn-primary pull-right" value="Ingresar">
    </form>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=BASE_URL?>js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=BASE_URL?>js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=BASE_URL?>datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=BASE_URL?>datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>js/app.js"></script>
  </body>
</html>
<?php session_start() ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="/incidencia/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

        <?php
        include 'menu.php';
        include 'conexion.php';
          //se conecta a la base de datos y se realiza la consulta

          switch ($_SESSION["rol"]) {
            case 'administrador':
              $sql = "select *, e.nombre as nom from ticket t
                      inner join estados e on t.id_estado = e.id_estado
                      inner join usuario u on t.id_usuario = u.id_usuario
                      order by t.fechayhora_t desc
                      ";
              break;

              case 'tester':
              $sql = "select *, e.nombre as nom from ticket t
                      inner join estados e on t.id_estado = e.id_estado
                      inner join usuario u on t.id_usuario = u.id_usuario
                      where u.id_usuario = " . $_SESSION["id_usuario"] . "
                      order by t.fechayhora_t desc";

              break;

              case 'tecnico':
              break;
          }



        $query = mysqli_query($conexion, $sql);
    ?>

  <div class="container">
      <form class="col-md-12 well" action="index.html" method="post">
	       <div class="row">
            <div class="col-md-3">
                <form action="#" method="get">
                    <div class="input-group">
                        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                        <input class="form-control" id="system-search" name="q" placeholder="Buscar.." required>
                        <span class="input-group-btn">

                        </span>
                    </div>
                </form>
            </div>
        		<div class="col-md-9">
            	  <table id="example1" class="table table-list-search">
                            <thead>
                                <tr>
                                    <th>Nro</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <th>Responsable</th>
                                    <th>Ultima Gestion</th>
                                    <th>Ver</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php while($row = mysqli_fetch_assoc($query)): ?>
                                    <tr>
                                    <td><?=$row['id_ticket']?></td>
                                    <td><?=$row['fechayhora_t'] ?></td>
                                    <td><?=$row['nom']?></td>
                                    <td><?=$row['nombre_u'] . ' ' . $row['apellido']?></td>
                                    <td></td>
                                    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                                    </tr>

                                <?php endwhile?>
                            </tbody>
                  </table>

                  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Detalle de Ticket</h4>
                              </div>

                              <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Nro Ticket</label>
                                            <input type="tel" class="form-control" name="phone" placeholder="" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Fecha</label>
                                            <input type="tel" class="form-control" name="phone" placeholder="" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Tipo de Insidencia</label>
                                            <input type="tel" class="form-control" name="phone" placeholder="" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="subject">Estado</label>
                                            <input type="tel" class="form-control" name="phone" placeholder="" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <div class="form-group">
                                              <label for="phone">Asunto</label>
                                              <input type="tel" class="form-control" name="phone" placeholder="" disabled>
                                          </div>
                                            <label for="message">Detalle</label>
                                            <textarea class="form-control" name="message" rows="11" placeholder="Descripcion detallada de Insidencia"disabled></textarea>
                                        </div>
                                    </div>
                                </div>

                                <table class="table table-list-search">
                                            <thead>
                                                <tr>
                                                    <th>Nro</th>
                                                    <th>Fecha</th>
                                                    <th>Estado</th>
                                                    <th>Responsable</th>
                                                    <th>Ultima Gestion</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>001</td>
                                                    <td>12-11-2011 11:11</td>
                                                    <td>Abierto</td>
                                                    <td>Juan Diaz</td>
                                                    <td>12-11-2011 11:11</td>
                                                </tr>
                                                <tr>
                                                    <td>001</td>
                                                    <td>12-11-2011 11:11</td>
                                                    <td>Abierto</td>
                                                    <td>Juan Diaz</td>
                                                    <td>12-11-2011 11:11</td>
                                                </tr>

                                            </tbody>
                                  </table>

                              </div>
                              <div class="modal-footer ">
                                <button type="button" class="btn btn-primary" style="">Agregar Gestion</button>
                                <a href="escalarTicket.php"><button type="button" class="btn btn-primary" style="">Escalar Ticket </button></a>

                              </div>
                        </div>
                      </div>
                    </div>

        		</div>
	</div>
  </form>
</div><!-- Container -->






    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="/incidencia/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/incidencia/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  </body>
  <script>
    $(document).ready(function (){
       $('#example1').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });

    })

  </script>
</html>

$(document).ready(function(){

          BASE_URL = $('base').attr('href');

          $('#example1').DataTable({
            "language": {
                "order": [[ 1, "desc" ]],
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontraron resultados en su busqueda",
                "searchPlaceholder": "Buscar registros",
                "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
              }
          });

          //modal
          $("body").on("click",".btn-view-ticket", function(){
              var ticket_id = $(this).attr('data-id');
              $.ajax({
                url  : BASE_URL + 'ver_ticket_ajax.php',
                type : 'post',
                data : {ticket_id : ticket_id},
                success: function(response){
                  $('#guardar-gestion').attr('data-id',0);
                  $('#guardar-escalar').attr('data-id',0);
                  cargar_modal(response);
                }
              })

              $('#edit').modal();
          });


          function cargar_modal(response){
              var ticket    = response.ticket
              var gestiones = response.gestiones
              var estados   = response.estados

              $('#guardar-gestion').attr('data-id',ticket.id_ticket);
              $('#guardar-escalar').attr('data-id',ticket.id_ticket);
              $('#guardar-escalar-tecnico').attr('data-id',ticket.id_ticket);
              $('#num').val(ticket.id_ticket);
              $('#fec').val(ticket.fechayhora_t);
              $('#tipo_insid').val(ticket.tipo_insid);
              $('#est').val(ticket.nom);
              $('#res').val(ticket.nombre_u + ' ' + ticket.apellido);
              $('#list-gestiones').empty();

              $('#est').empty();
              for(e of estados){
                let html = `<option>${e.nombre}</option>`
                html = ticket.nom == e.nombre ? `<option value="${e.id_estado}" selected>${e.nombre}</option>` : `<option value="${e.id_estado}">${e.nombre}</option>`;
                $('#est').append(html);
              }

              for(g of gestiones){
                trs =`<tr>
                        <td>${g.id_gestion}</td>
                         <td>${g.fechayhora_g}</td>
                         <td>${g.tipo_gestion}</td>
                         <td>${g.nombre_u + ' ' + g.apellido}</td>
                        <td><button target-id="details-${g.id_gestion}" class="badge collapser">detalle</button></td>
                      </tr>
                      <tr id="details-${g.id_gestion}" class="collapse">
                        <td colspan="5">
                          <div class="col-md-12"><b>Asunto:</b> <span>${g.asunto}</span></div>
                          <br>
                          <div class="col-md-12"><br><p>${g.detalle}</p></div>
                        </td>
                      </tr>`;
                $('#list-gestiones').append(trs);
              }

          }


          //collapser
          $('body').on('click','.collapser',function(){
            $('#'+$(this).attr('target-id')).toggle();
          })

          //close mmodal
          $('.dismiss-modal').click(function(){
            $('#guardar-gestion').attr('data-id',0);
            $('#guardar-escalar').attr('data-id',0);
            $('#edit').modal('hide')
            location.reload();
          })


          //guardar gestion
          $('#guardar-gestion').click(function(){
            id_ticket = $(this).attr('data-id');
            id_estado = $('#est option:selected').val();
            asunto    = $('#asunto_gestion').val();
            detalle   = $('#detalle_gestion').val();
            
            if(id_ticket > 0 && id_estado > 0 && asunto != '' && detalle != '')
            {
              $.ajax({
                url : BASE_URL + 'agregar_gestion_ajax.php',
                type : 'post',
                data : {id_ticket : id_ticket, id_estado : id_estado , asunto : asunto , detalle : detalle},
                success: function(response){
                  console.log(response.status)
                  if(response.status)
                  {
                    $('.gestion-ok').css('visibility','visible');
                    setTimeout(function(){
                      $('.gestion-ok').css('visibility','hidden');
                      $('#asunto_gestion').val('');
                      $('#detalle_gestion').val('');
                      $('#edit').modal('hide')
                    },3000)
                  }
                }
              })
            }
            else
            {
              alert('Ingresar un asunto y un detalle');
            }
          })


          //guardar escalr
          $('#guardar-escalar').click(function(){
            id_ticket  = $(this).attr('data-id');
            id_estado  = $('#est option:selected').val();
            id_tecnico = $('#id_tecnico option:selected').val();
            asunto     = $('#asunto_escalar').val();
            detalle    = $('#detalle_escalar').val();
            
            if(id_ticket > 0 && id_estado > 0 && id_tecnico > 0 && asunto != '' && detalle != '')
            {
              $.ajax({
                url : BASE_URL + 'agregar_escalar_ajax.php',
                type : 'post',
                data : {id_ticket : id_ticket, id_estado : id_estado ,id_tecnico : id_tecnico, asunto : asunto , detalle : detalle},
                success: function(response){
                  console.log(response.status)
                  if(response.status)
                  {
                    $('.escalar-ok').css('visibility','visible');
                    setTimeout(function(){
                      $('.escalar-ok').css('visibility','hidden');
                      $('#asunto_escalar').val('');
                      $('#detalle_escalar').val('');
                      $('#edit').modal('hide')
                    },3000)
                  }
                }
              })
            
            }
            else
            {
              alert('Ingresar un asunto y un detalle');
            }
          })

          //guardar escalar tecnico
          $('#guardar-escalar-tecnico').click(function(){
            id_estado  = $('#est option:selected').val();
            id_ticket  = $('#guardar-escalar-tecnico').attr('data-id');
            
            if( id_estado > 0 )
            {
              $.ajax({
                url : BASE_URL + 'agregar_escalar_tec_ajax.php',
                type : 'post',
                data : {id_estado : id_estado,id_ticket: id_ticket },
                success: function(response){
                  response = JSON.parse(response)
                  console.log(response.status)
                  debugger
                  if(response.status)
                  {
                    $('.escalar-ok').css('visibility','visible');
                    //location.reload();
                  }
                }
              })
            }

            else
            {
              alert('Elija un estado');
            }
          })

        })

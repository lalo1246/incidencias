<?php
  session_start();
  if(!isset($_SESSION['id_usuario']))
    header('location:index.php');
?>
<?php include 'conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?=BASE_URL?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URL?>css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=BASE_URL?>datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <base href="<?=BASE_URL?>">
  </head>
  <body>
  <?php include 'menu.php';?>
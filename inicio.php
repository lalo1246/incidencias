<?php include 'header.php' ?>
<div class="container">
  <form class="col-md-12">
    <div class="jumbotron">
      <h1>Gestion de Incidencias</h1>
      <p>La Gestión de Incidencias tiene como objetivo resolver cualquier incidente que cause una interrupción en el
        servicio de la manera más rápida y eficaz posible.
        <br>
        Sistema de Gestion de Incidencias realizado por alumnos de la UTN FRT para la materia Programación Web.
        <br><b>Alumno</b>
        <br><i class="fa fa-user-circle-o" aria-hidden="true"></i>Suarez Alejandro (28917)
      </p>
    </div>
  </form>
</div>

<?php include 'footer.php' ?>

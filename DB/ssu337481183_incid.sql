-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 03, 2018 at 07:02 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u337481183_incid`
--

-- --------------------------------------------------------

--
-- Table structure for table `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(25) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estados`
--

INSERT INTO `estados` (`id_estado`, `nombre`) VALUES
(1, 'iniciado'),
(2, 'en proceso'),
(3, 'finalizado ');

-- --------------------------------------------------------

--
-- Table structure for table `gestion`
--

CREATE TABLE `gestion` (
  `id_gestion` int(5) NOT NULL,
  `asunto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detalle` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_gestion` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fechayhora_g` datetime NOT NULL,
  `id_ticket` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `ultima` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gestion`
--

INSERT INTO `gestion` (`id_gestion`, `asunto`, `detalle`, `tipo_gestion`, `fechayhora_g`, `id_ticket`, `id_usuario`, `ultima`) VALUES
(1, 'Se rompio el CPU', 'El cpu no enciende, desconozco ', 'Apertura de Ticket', '2017-02-17 22:32:56', 1, 1, 1),
(2, 'Soporte de hardware', 'Reparación fuente almacenamiento: Solicita reparación electrónica de fuente de almacenamiento de PC', 'Apertura de Ticket', '2017-02-17 23:47:09', 2, 2, 1),
(3, 'Actualización de silverlight', 'Se necesita el permiso de Administrador para poder realizar una actualización de Microsoft Silverlight para poder ejecutar la aplicación de trabajo.', 'Apertura de Ticket', '2017-02-17 10:53:49', 3, 5, 1),
(4, 'La notebook no enciende', 'La notebook no enciende de ninguna forma. ', 'Apertura de Ticket', '2017-02-17 10:57:13', 4, 5, 1),
(5, 'Instalación de software', 'Instalación del aplicativo SIAPRE para realizar trabajo contable.', 'Apertura de Ticket', '2017-02-17 10:58:53', 5, 5, 1),
(6, 'adfa', 'sdfasdf', 'Apertura de Ticket', '2017-03-17 12:47:18', 6, 1, 1),
(7, 'kljjh', 'jkhjk', 'Apertura de Ticket', '2017-05-17 00:03:34', 7, 1, 1),
(8, 'vsfdgfdgsfdg', 'fsadfasdfasdf', 'Apertura de Ticket', '2017-05-17 20:47:43', 8, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(5) NOT NULL,
  `rol` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`) VALUES
(1, 'administrador'),
(2, 'tester'),
(3, 'usuario');

-- --------------------------------------------------------

--
-- Table structure for table `sector`
--

CREATE TABLE `sector` (
  `id_sector` int(5) NOT NULL,
  `nombre_s` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `oficina` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sector`
--

INSERT INTO `sector` (`id_sector`, `nombre_s`, `oficina`) VALUES
(1, 'incidencias', 'informatica'),
(2, 'inventario', 'administracion'),
(3, 'gerencia', 'gerencia');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(5) NOT NULL,
  `fechayhora_t` datetime NOT NULL,
  `dispositivo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_insid` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `prioridad` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_estado` int(25) NOT NULL,
  `id_sector` int(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `fechayhora_t`, `dispositivo`, `categoria`, `tipo_insid`, `prioridad`, `id_estado`, `id_sector`) VALUES
(1, '2017-02-17 22:32:56', 'AA17001', 'Hardware', 'Reparacion', 'Media', 1, 0),
(2, '2017-02-17 23:47:09', 'AA1000', 'Hardware', 'Reparacion', 'Media', 1, 0),
(3, '2017-02-17 10:53:49', 'NOTE100', 'Software', 'Otro', 'Alta', 1, 0),
(4, '2017-02-17 10:57:13', 'NOTE110', 'Hardware', 'Reparacion', 'Critica', 1, 0),
(5, '2017-02-17 10:58:53', 'NOTE105', 'Software', 'Otro', 'Alta', 1, 0),
(6, '2017-03-17 12:47:18', 'asassa', 'Software', 'Reparacion', 'Media', 1, 0),
(7, '2017-05-17 00:03:34', '2311322', 'Hardware', 'Reparacion', 'Media', 1, 0),
(8, '2017-05-17 20:47:43', '1345rfgs', 'Hardware', 'Reparacion', 'Media', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(5) NOT NULL,
  `apellido` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_u` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_sector` int(5) NOT NULL,
  `id_rol` int(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `apellido`, `nombre_u`, `email`, `login`, `password`, `id_sector`, `id_rol`) VALUES
(1, 'Paliza', 'Cesar', 'cpaliza@gmail.com', 'cpaliza', '1234', 1, 1),
(2, 'Ibañez', 'Sofia', 'sibañez@gmail.com', 'siabenz', '1234', 1, 1),
(3, 'Salazar', 'Alejandro', 'asalazar@gmail.com', 'asalazar', '1234', 1, 1),
(4, 'Aguirre', 'Ana', 'aaguirre@gmail.com', 'aaguirre', '1234', 1, 1),
(5, 'Paez', 'Matias', 'mpaez@gmail.com', 'mpaez', '1234', 1, 1),
(6, 'Ruiz', 'Mariana', 'mruiz@gmail.com', 'mruiz', '1234', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indexes for table `gestion`
--
ALTER TABLE `gestion`
  ADD PRIMARY KEY (`id_gestion`),
  ADD KEY `id_ticket` (`id_ticket`,`id_usuario`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`),
  ADD KEY `id_usuario` (`id_rol`);

--
-- Indexes for table `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`id_sector`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`),
  ADD KEY `id_estado` (`id_estado`),
  ADD KEY `id_sector` (`id_sector`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_sector` (`id_sector`),
  ADD KEY `id_sector_2` (`id_sector`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `estados`
--
ALTER TABLE `estados`
  MODIFY `id_estado` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gestion`
--
ALTER TABLE `gestion`
  MODIFY `id_gestion` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sector`
--
ALTER TABLE `sector`
  MODIFY `id_sector` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

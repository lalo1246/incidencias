-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-11-2018 a las 12:26:43
-- Versión del servidor: 5.7.24-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u337481183_incid`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(25) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id_estado`, `nombre`) VALUES
(1, 'iniciado'),
(2, 'en proceso'),
(3, 'finalizado ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestion`
--

CREATE TABLE `gestion` (
  `id_gestion` int(5) NOT NULL,
  `asunto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detalle` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_gestion` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fechayhora_g` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_ticket` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `gestion`
--

INSERT INTO `gestion` (`id_gestion`, `asunto`, `detalle`, `tipo_gestion`, `fechayhora_g`, `id_ticket`, `id_usuario`) VALUES
(1, 'Se rompio el CPU', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2017-02-17 22:32:56', 1, 1),
(22, 'asunto 4', 'nuevo 4', 'Nueva gestion', '2018-11-08 10:45:15', 4, 1),
(23, 'otra gstion', 'des getion', 'Nueva gestion', '2018-11-08 10:45:54', 4, 1),
(24, 'no tenia aá¹•ertura', 'ahora si', 'Nueva gestion', '2018-11-08 10:46:25', 4, 1),
(25, 'dhfdGFD', 'FGD', 'Nueva gestion', '2018-11-08 10:47:22', 4, 1),
(17, 'Ã±lklk', 'kjhhjkhkj', 'Nueva gestion', '2018-11-08 10:25:52', 21, 1),
(18, 'otra gesion', 'lorem ipsum gestion nw', 'Nueva gestion', '2018-11-08 10:28:51', 21, 1),
(19, 'se volvi a romper', 'el coso del baÃ±o', 'Nueva gestion', '2018-11-08 10:30:27', 21, 1),
(20, 'en proceso', 'gestin en process', 'Nueva gestion', '2018-11-08 10:40:30', 1, 1),
(21, 'en proceso', 'gestin en process', 'Nueva gestion', '2018-11-08 10:40:42', 1, 1),
(4, 'La notebook no enciende', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2017-02-17 10:57:13', 4, 5),
(5, 'Instalación de software', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2017-02-17 10:58:53', 5, 5),
(6, 'adfa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2017-03-17 12:47:18', 6, 1),
(7, 'kljjh', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2017-05-17 00:03:34', 7, 1),
(8, 'vsfdgfdgsfdg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2017-05-17 20:47:43', 8, 1),
(9, 'fgfgf', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-03 21:35:20', 18, 6),
(10, 'fgfgf', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-03 21:36:43', 19, 6),
(11, 'fgfgf', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-03 21:37:48', 20, 6),
(12, 'fgfgf', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-03 21:39:11', 21, 6),
(13, 'testing 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-05 16:33:15', 22, 6),
(14, 'testing 21', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-05 16:33:15', 22, 6),
(15, 'testing 22', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-05 16:33:15', 22, 6),
(16, 'titulo', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. a qui officia deserunt mollit anim id est laborum.', 'Apertura de Ticket', '2018-11-07 16:06:30', 23, 6),
(26, 'dfg', 'sgd', 'Nueva gestion', '2018-11-08 10:48:34', 4, 1),
(27, 'XCVXs', 'sdfsdf', 'Nueva gestion', '2018-11-08 10:50:53', 16, 1),
(28, 'dfsdf', 'sdfsdfsdfs', 'Nueva gestion', '2018-11-08 10:51:34', 16, 1),
(29, 'pasa a proceso', 'proceso', 'Nueva gestion', '2018-11-08 10:52:22', 16, 1),
(30, 'ahora si', 'en proceso', 'Nueva gestion', '2018-11-08 10:55:23', 16, 1),
(31, 'fin', 'fin', 'Nueva gestion', '2018-11-08 10:56:09', 16, 1),
(32, 'fin', 'fi', 'Nueva gestion', '2018-11-08 10:57:08', 16, 1),
(33, 'asunton7', 'sdfs', 'Nueva gestion', '2018-11-08 10:59:25', 7, 1),
(34, 'fin', 'fin', 'Nueva gestion', '2018-11-08 10:59:58', 6, 1),
(35, 'pal tecncio', 'tecnco', 'Escalar Ticket', '2018-11-08 11:16:06', 1, 1),
(36, 'esc', 'esc', 'Escalar Ticket', '2018-11-08 11:18:03', 4, 1),
(37, 'esc', 'esc', 'Escalar Ticket', '2018-11-08 11:19:10', 8, 1),
(38, 'gestion 9', 'gestion', 'Nueva gestion', '2018-11-08 11:21:06', 9, 1),
(39, 'proceso 9', 'proces', 'Nueva gestion', '2018-11-08 11:21:22', 9, 1),
(40, 'fin', 'fin escalr', 'Escalar Ticket', '2018-11-08 11:21:46', 9, 1),
(41, 'proc', 'proc', 'Nueva gestion', '2018-11-08 11:23:19', 12, 1),
(42, 'inciado', 'iniciado', 'Nueva gestion', '2018-11-08 11:24:51', 10, 1),
(43, 'proceso', 'proceso', 'Nueva gestion', '2018-11-08 11:25:22', 10, 1),
(44, 'final', 'final', 'Escalar Ticket', '2018-11-08 11:25:49', 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(5) NOT NULL,
  `rol` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`) VALUES
(1, 'administrador'),
(2, 'tester'),
(3, 'tecnico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sector`
--

CREATE TABLE `sector` (
  `id_sector` int(5) NOT NULL,
  `nombre_s` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `oficina` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sector`
--

INSERT INTO `sector` (`id_sector`, `nombre_s`, `oficina`) VALUES
(1, 'incidencias', 'informatica'),
(2, 'inventario', 'administracion'),
(3, 'gerencia', 'gerencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(5) NOT NULL,
  `fechayhora_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dispositivo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_insid` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `prioridad` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_estado` int(25) NOT NULL,
  `id_sector` int(25) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_tecnico` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `fechayhora_t`, `dispositivo`, `categoria`, `tipo_insid`, `prioridad`, `id_estado`, `id_sector`, `id_usuario`, `id_tecnico`) VALUES
(1, '2017-02-17 22:32:56', 'AA17001', 'Hardware', 'Reparacion', 'Media', 3, 1, 5, 2),
(4, '2017-02-17 10:57:13', 'NOTE110', 'Hardware', 'Reparacion', 'Critica', 3, 1, 5, 2),
(6, '2017-03-17 12:47:18', 'asassa', 'Software', 'Reparacion', 'Media', 3, 1, 5, NULL),
(7, '2017-05-17 00:03:34', '2311322', 'Hardware', 'Reparacion', 'Media', 1, 1, 5, NULL),
(8, '2017-05-17 20:47:43', '1345rfgs', 'Hardware', 'Reparacion', 'Media', 3, 1, 5, 2),
(9, '2018-11-03 21:21:34', 'asd', 'Hardware', 'Reparacion', 'Media', 3, 1, 6, 3),
(10, '2018-11-03 21:21:52', 'asd', 'Hardware', 'Reparacion', 'Media', 3, 1, 6, 3),
(11, '2018-11-03 21:22:13', 'asd', 'Hardware', 'Reparacion', 'Media', 2, 1, 6, NULL),
(12, '2018-11-03 21:31:50', 'asd', 'Hardware', 'Reparacion', 'Media', 2, 1, 6, NULL),
(13, '2018-11-03 21:32:26', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 6, NULL),
(14, '2018-11-03 21:32:50', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 6, NULL),
(15, '2018-11-03 21:33:48', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 6, NULL),
(16, '2018-11-03 21:34:21', 'asd', 'Hardware', 'Reparacion', 'Media', 3, 1, 3, NULL),
(17, '2018-11-03 21:34:27', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 3, NULL),
(18, '2018-11-03 21:35:20', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 3, NULL),
(19, '2018-11-03 21:36:43', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 3, NULL),
(20, '2018-11-03 21:37:48', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 3, NULL),
(21, '2018-11-03 21:39:11', 'asd', 'Hardware', 'Reparacion', 'Media', 1, 1, 3, NULL),
(22, '2018-11-05 16:33:15', 'lg leon', 'Hardware', 'Reparacion', 'Critica', 1, 1, 6, NULL),
(23, '2018-11-07 16:06:30', 'lg leon', 'Hardware', 'Reparacion', 'Media', 1, 1, 6, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(5) NOT NULL,
  `apellido` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_u` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_sector` int(5) NOT NULL,
  `id_rol` int(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `apellido`, `nombre_u`, `email`, `login`, `password`, `id_sector`, `id_rol`) VALUES
(1, 'Paliza', 'Cesar', 'cpaliza@gmail.com', 'cpaliza', '1234', 1, 1),
(2, 'Ibañez', 'Sofia', 'sibañez@gmail.com', 'siabenz', '1234', 1, 3),
(3, 'Salazar', 'Alejandro', 'asalazar@gmail.com', 'asalazar', '1234', 1, 3),
(4, 'Aguirre', 'Ana', 'aaguirre@gmail.com', 'aaguirre', '1234', 1, 1),
(5, 'Paez', 'Matias', 'mpaez@gmail.com', 'mpaez', '1234', 1, 1),
(6, 'Ruiz', 'Mariana', 'mruiz@gmail.com', 'mruiz', '1234', 1, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `gestion`
--
ALTER TABLE `gestion`
  ADD PRIMARY KEY (`id_gestion`),
  ADD KEY `id_ticket` (`id_ticket`,`id_usuario`),
  ADD KEY `id_ticket_2` (`id_ticket`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`),
  ADD KEY `id_usuario` (`id_rol`);

--
-- Indices de la tabla `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`id_sector`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`),
  ADD KEY `id_estado` (`id_estado`),
  ADD KEY `id_sector` (`id_sector`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_sector` (`id_sector`),
  ADD KEY `id_sector_2` (`id_sector`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id_estado` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `gestion`
--
ALTER TABLE `gestion`
  MODIFY `id_gestion` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT de la tabla `sector`
--
ALTER TABLE `sector`
  MODIFY `id_sector` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

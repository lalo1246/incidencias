<?php include 'header.php' ?>
  
<?php



  //se conecta a la base de datos y se realiza la consulta
  switch ($_SESSION["rol"]) {
    case 'administrador':
      $sql = "select u.*,t.id_ticket,t.fechayhora_t,s.nombre_s, e.nombre as nom, v.nombre_u as nombre_t from ticket t
      inner join estados e on t.id_estado = e.id_estado
      inner join usuario u on t.id_usuario = u.id_usuario
      inner join sector s on s.id_sector = u.id_sector
      left join usuario v on t.id_tecnico = v.id_usuario
      order by t.fechayhora_t desc
    ";
    break;
    case 'tester':
      $sql = "select u.*,t.id_ticket,t.fechayhora_t,s.nombre_s, e.nombre as nom, v.nombre_u as nombre_t from ticket t
      inner join estados e on t.id_estado = e.id_estado
      inner join usuario u on t.id_usuario = u.id_usuario
      inner join sector s on s.id_sector = u.id_sector
      left join usuario v on t.id_tecnico = v.id_usuario
      where u.id_usuario = " . $_SESSION["id_usuario"] . "
      order by t.fechayhora_t desc";
    break;
    case 'tecnico':
      $sql = "select u.*,t.id_ticket,t.fechayhora_t,s.nombre_s, e.nombre as nom, v.nombre_u as nombre_t from ticket t
      inner join estados e on t.id_estado = e.id_estado
      inner join usuario u on t.id_usuario = u.id_usuario
      inner join sector s on s.id_sector = u.id_sector
      left join usuario v on t.id_tecnico = v.id_usuario
      where t.id_tecnico = " . $_SESSION["id_usuario"] . "
      order by t.fechayhora_t desc";    
    break;
  }
  $query = mysqli_query($conexion, $sql);
?>
<body></body>
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <table id="example1" class="table table-list-search">
        <thead>
          <tr>
            <th>Nro</th>
            <th>Fecha de Creacion</th>
            <th>Estado</th>
            <th>Responsable</th>
            <th>Asignado a</th>
            <th>Sector</th>
            <th>Ver</th>
          </tr>
        </thead>
        <tbody>
          <?php while ($row = mysqli_fetch_assoc($query)): ?>
          <tr>
            <td><?=$row['id_ticket']?></td>
            <td><?=$row['fechayhora_t']?></td>
            <td> <?=$row['nom']?></td>
            <td> <?=$row['nombre_u'] . ' ' . $row['apellido']?></td>
            <td> <?= $row['nombre_t'] == null ? 'administrador' : $row['nombre_t']  ?></td>
            <td> <?=$row['nombre_s']?></td>
            <td><p data-placement="top"><button class="btn btn-primary btn-xs btn-view-ticket" data-id="<?=$row['id_ticket']?>"><span class="glyphicon glyphicon-pencil"></span></button></p></td>
          </tr>
          <?php endwhile?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- Container -->
  
<!-- mmodal -->
<div class="modal fade " id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Detalle de Ticket</h4>
      </div>
      <div class="modal-body">
        <div id="tabla" class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="name">Nro Ticket</label>
              <input id="num" type="text" class="form-control" name="Ticket" placeholder="" disabled>
            </div>
            <div class="form-group">
              <label for="email">Fecha</label>
              <input id="fec" type="text" class="form-control" name="Fecha" placeholder="" disabled>
            </div>
            <div class="form-group">
              <label for="phone">Tipo de Insidencia</label>
              <input id="tipo_insid" type="text" class="form-control" name="Incidencia" placeholder="" disabled>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="subject">Estado</label>
              <select id="est" type="text" class="form-control" name="phone" <?=!in_array($_SESSION['rol'],['administrador','tecnico']) ? 'disabled' : '' ?>>
              </select>
            </div>
            <div class="form-group">
              <label for="subject">Responsable</label>
              <input id="res" type="text" class="form-control" name="Responsable" placeholder="" disabled>
            </div>
      
      <?php if (in_array($_SESSION['rol'],['tecnico'])):  ?>
        <div class="modal-footer ">
          <button class="btn btn-primary pull-right dismiss-modal" id="guardar-escalar-tecnico" name="phpmailer">Aceptar</button>
          <button class="btn btn-default pull-right dismiss-modal" style="margin-right:10px">Cancelar</button>          
        </div>
      <?php endif; ?>

          </div>
        </div>
        <div class="alert alert-info" role="alert">
          Gestiones realizadas
        </div>
        <table class="table table-list-search">
          <thead>
            <tr>
              <th>Nro</th>
              <th>Fecha</th>
              <th>Tipo</th>
              <th>Responsable</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="list-gestiones">
          </tbody>
        </table>
        <?php if (in_array($_SESSION['rol'],['administrador'])): ?>
        <hr>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="home">
            <div class="alert alert-info" role="alert">
              Gestionar Ticket
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Asunto</label>
                  <input id="asunto_gestion" type="text" class="form-control" maxlength="49">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Detalle</label>
                  <textarea id="detalle_gestion" class="form-control" rows="6" maxlength="299"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <span class="glyphicon glyphicon-ok pull-right gestion-ok" aria-hidden="true" style="margin: 10px 10px;color:#2ab32c;visibility: hidden"></span>
                <button class="btn btn-primary pull-right" id="guardar-gestion">Aceptar</button>
                <button class="btn btn-default pull-right dismiss-modal" style="margin-right:10px">Cancelar</button>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="profile">
            <div class="alert alert-info" role="alert">
              Escalar Ticket
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Asunto</label>
                  <input id="asunto_escalar" type="text" class="form-control" name="phone" maxlength="49">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Detalle</label>
                  <textarea id="detalle_escalar" class="form-control" rows="6" maxlength="299"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Nombre tecnico</label>
                  <select id="id_tecnico" class="form-control" name="">
                    <?php $sql = "select * from usuario where id_rol = 3" ?>
                    <?php $query = mysqli_query($conexion, $sql); ?>
                    <?php while ($row = mysqli_fetch_assoc($query)): ?>
                    <option value="<?=$row['id_usuario']?>"><?=$row['nombre_u'].' '.$row['apellido']?></option>
                    <?php endwhile ?>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <span class="glyphicon glyphicon-ok pull-right escalar-ok" aria-hidden="true" style="margin: 10px 10px;color:#2ab32c;visibility: hidden"></span>
                <button class="btn btn-primary pull-right" id="guardar-escalar">Aceptar</button>
                <button class="btn btn-default pull-right dismiss-modal" style="margin-right:10px">Cancelar</button>
              </div>
            </div>
          </div>
        </div>
        <?php endif; ?>
      </div>
      <?php if (in_array($_SESSION['rol'],['administrador','tester'])): ?>
      <div class="modal-footer ">
        <!-- Nav tabs -->
        <ul class="nav nav-pills" role="tablist">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Gestionar</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Escalar</a></li>
        </ul>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<!-- modal -->

<?php include 'footer.php' ?>
